<?php
abstract class Customer
{
    private $customer_id;
    private $name;
    private $surname;
    private $secondSurname;
    private $taxId;
    private $phoneNumber;
    private $emailAddress;
    private $address;
    private $accountCode;
    private $income;
    private $isVip;

    function __construct($customer_id, $name, $surname, $second_surname, $tax_id, $phone_number, $email_address, $address, $account_code, $income, $is_vip)
    {
        $this->customer_id = $customer_id;
        $this->name = $name;
        $this->surname = $surname;
        $this->secondSurname = $second_surname;
        $this->taxId = $tax_id;
        $this->phoneNumber = $phone_number;
        $this->emailAddress = $email_address;
        $this->address = $address;
        $this->accountCode = $account_code;
        $this->income = $income;
        $this->isVip = $is_vip;
    }

    public function getCustomerId()
    {
        return $this->customer_id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getSurname()
    {
        return $this->surname;
    }
    public function getSecondSurname()
    {
        return $this->secondSurname;
    }
    public function getTaxId()
    {
        return $this->taxId;
    }
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }
    public function getAddress()
    {
        return $this->address;
    }
    public function getAccountCode()
    {
        return $this->accountCode;
    }
    public function getIncome()
    {
        return $this->income;
    }
    public function getIsVip()
    {
        return $this->isVip;
    }

    public function getTotalIncome(){
        return $this->income;
    }
}
