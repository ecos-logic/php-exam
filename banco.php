<?php
    require_once './VipCustomer.php';
    require_once './NormalCustomer.php';
    require_once './DatabaseActions.php';
    
    $databaseAccess = new DatabaseActions('localhost','bank','bank_user','bank_user');
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customers</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body style="margin: 10px;">
<?php
    $clientes = $databaseAccess->getClientes();    
?>
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Income</th>
        </tr>
    <?php
        foreach ($clientes as $cliente) {
    ?>
    <tr
    
    <?php   
            if ($cliente->getIsVip()) {
            ?>
                style="background-color: bisque;"
            <?php
            }
            ?>

    >
        <td><?=$cliente->getName()?> <?=$cliente->getSurname()?> <?=$cliente->getSecondSurname()?></td>
        <td><?=$cliente->getTotalIncome()?></td>
    </tr>    
    <?php
        }
    ?>
    </thead>
</table>
    
</body>
</html>
