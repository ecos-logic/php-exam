<?php
require_once './VipCustomer.php';
require_once './NormalCustomer.php';

class DatabaseActions
{
    private $host;
    private $username;
    private $password;
    private $databaseName;

    public function __construct($host,$databaseName,$username,$password){
        $this->host = $host;
        $this->databaseName = $databaseName;
        $this->username = $username;
        $this->password = $password;


    }
    private function connect():bool|mysqli
    {
        $dbc = mysqli_connect($this->host, $this->username, $this->password, $this->databaseName);
        if (!$dbc) {
            return false;
        }
        return $dbc;
    }

    public function add(VipCustomer|NormalCustomer $customer):bool
    {
        $dbc = $this->connect();
        if (!$dbc) { 
            return false;
        }
        $name = $customer->getName();
        $surname = $customer->getSurname();
        $secondSurname = $customer->getSecondSurname();
        $tax_id = $customer->getTaxId();
        $email = $customer->getEmailAddress();
        $phone = $customer->getPhoneNumber();
        $address = $customer->getAddress();
        $accountCode = $customer->getAccountCode();
        $income = $customer->getIncome();
        $is_vip = $customer->getIsVip() ? 1 : 0;
        $shareValue = 0;
        $promotion = '';
        if ($is_vip) {
            $shareValue = $customer->getShareValue();
        } else {
            $promotion = $customer->getPromotion();
        }

        $query = "
    insert into 
        customer 
    (
        name,
        surname,
        second_surname,
        tax_id,
        email_address,
        phone_number,
        address,
        account_code,
        income,
        is_vip,
        share_value,
        promotion
    ) values 
    (
        '$name',
        '$surname',
        '$secondSurname',
        '$tax_id',
        '$email',
        '$phone',
        '$address',
        '$accountCode',
        '$income',
        $is_vip,
        '$shareValue',
        '$promotion');
    ";
        
        mysqli_query($dbc, $query);
        return true;
    }

    public function getClientes():array
    {
        $dbc = $this->connect();


        $query = 'select * from customer';


        $queryResult = mysqli_query($dbc, $query);
        if (!$queryResult) {
            return array();
        }

        $result = array();
        while ($row = mysqli_fetch_array($queryResult)) {
            if ($row['is_vip']) {
                $result[] = new VipCustomer(
                    $row['customer_id'],
                    $row['name'],
                    $row['surname'],
                    $row['second_surname'],
                    $row['tax_id'],
                    $row['phone_number'],
                    $row['email_address'],
                    $row['address'],
                    $row['account_code'],
                    $row['income'],
                    $row['share_value']
                );
            } else {
                $result[] = new NormalCustomer(
                    $row['customer_id'],
                    $row['name'],
                    $row['surname'],
                    $row['second_surname'],
                    $row['tax_id'],
                    $row['phone_number'],
                    $row['email_address'],
                    $row['address'],
                    $row['account_code'],
                    $row['income'],
                    $row['promotion']
                );
            }
        }
        return $result;
    }
    public function getCliente($id):NormalCustomer|VipCustomer|null
    {
        $dbc = $this->connect();


        $query = "select * from customer WHERE customer_id = $id";


        $queryResult = mysqli_query($dbc, $query);
        if (!$queryResult) {
            return null;
        }

        $result = array();
        $row = mysqli_fetch_array($queryResult);
        if ($row['is_vip']) {
            $result = new VipCustomer(
                $row['customer_id'],
                $row['name'],
                $row['surname'],
                $row['second_surname'],
                $row['tax_id'],
                $row['phone_number'],
                $row['email_address'],
                $row['address'],
                $row['account_code'],
                $row['income'],
                $row['share_value']
            );
        } else {
            $result = new NormalCustomer(
                $row['customer_id'],
                $row['name'],
                $row['surname'],
                $row['second_surname'],
                $row['tax_id'],
                $row['phone_number'],
                $row['email_address'],
                $row['address'],
                $row['account_code'],
                $row['income'],
                $row['promotion']
            );
        }
        return $result;
    }    
}
