<?php
    $isNewCustomer = !isset($_GET['id']);
    if(!$isNewCustomer){
        $customerIdToRead = $_GET['id'];
        require_once './VipCustomer.php';
        require_once './NormalCustomer.php';
        require_once './DatabaseActions.php';
        
        $databaseAccess = new DatabaseActions('localhost','bank','bank_user','bank_user');
        $cliente = $databaseAccess->getCliente($customerIdToRead);
    }

?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customers</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body style="margin: 10px;">
    <form method="post" action="<?=isset($cliente)?"update_customer.php":"insert_customer.php"?>">
        <p><label>Name: </label> <input type="text" name="name" value="<?=isset($cliente)?$cliente->getName():""?>" /></p>
        <p><label>Surname: </label> <input type="text" name="surname" value="<?=isset($cliente)?$cliente->getSurname():""?>"/></p>
        <p><label>Second surname: </label> <input type="text" name="second_surname" value="<?=isset($cliente)?$cliente->getSecondSurname():""?>"/></p>
        <p><label>Tax id: </label> <input type="text" name="tax_id" value="<?=isset($cliente)?$cliente->getTaxId():""?>"/></p>
        <p><label>Phone: </label> <input type="text" name="phone_number" value="<?=isset($cliente)?$cliente->getPhoneNumber():""?>"/></p>
        <p><label>Email: </label> <input type="text" name="email_address" value="<?=isset($cliente)?$cliente->getEmailAddress():""?>"/></p>
        <p><label>Address: </label> <input type="text" name="address" value="<?=isset($cliente)?$cliente->getAddress():""?>"/></p>
        <p><label>Accound: </label> <input type="text" name="account_code" value="<?=isset($cliente)?$cliente->getAccountCode():""?>"/></p>
        <p><label>Income: </label> <input type="text" name="income" value="<?=isset($cliente)?$cliente->getIncome():""?>"/></p>
        <p><label>VIP?: </label> <input id="is_vip" type="checkbox" name="is_vip" onclick="onCheck()" <?=isset($cliente)?"disabled readonly":""?> <?=isset($cliente)?($cliente->getIsVip()?"checked":""):""?>/></p>
        <p id="promotion"><label>Promotion: </label> <input type="text" name="promotion" value="<?=isset($cliente)?($cliente->getIsVip()?"":$cliente->getPromotion()):""?>" /></p>
        <p id="share_value"><label>Share: </label> <input type="text" name="share_value" value="<?=isset($cliente)?($cliente->getIsVip()?$cliente->getShareValue():""):"0"?>"/></p>
        <input type="submit" value="Enviar"/>
    </form>
    <script>
        function onCheck() {
            if (document.getElementById("is_vip").checked){
                document.getElementById("share_value").style.visibility = "visible";
                document.getElementById("promotion").style.visibility = "hidden";
            }
            else{
                document.getElementById("share_value").style.visibility = "hidden";
                document.getElementById("promotion").style.visibility = "visible";
            }
        }

        onCheck();
    </script>
</body>
</html>