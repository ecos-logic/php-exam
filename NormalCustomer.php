<?php
require_once './Customer.php';

class NormalCustomer extends Customer{
    private $promotion;

    function __construct($customer_id, $name, $surname, $second_surname, $tax_id, $phone_number, $email_address, $address, $account_code, $income,  $promotion)
    {
        parent::__construct($customer_id, $name, $surname, $second_surname, $tax_id, $phone_number, $email_address, $address, $account_code, $income, false);
        $this->$promotion = $promotion;
    }

    public function getPromotion(){
        return $this->promotion;
    }

}