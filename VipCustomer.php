<?php
require_once './Customer.php';

class VipCustomer extends Customer{
    private $shareValue;

    function __construct($customer_id, $name, $surname, $second_surname, $tax_id, $phone_number, $email_address, $address, $account_code, $income, $share_value)
    {
        parent::__construct($customer_id, $name, $surname, $second_surname, $tax_id, $phone_number, $email_address, $address, $account_code, $income, true);
        $this->shareValue = $share_value;
    }

    public function getTotalIncome()
    {        
        return parent::getIncome() + $this->shareValue;
    }

    public function getShareValue(){
        return $this->shareValue;
    }
}